import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SubirPage } from '../pages/subir/subir';

//pipes
import {PipesModule} from "../pipes/pipes.module";

//plugins camara
import { Camera} from '@ionic-native/camera';
//imagepicker
import { ImagePicker } from '@ionic-native/image-picker';

//firebase
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
//provider
import { CargaArchivoProvider } from '../providers/carga-archivo/carga-archivo';


export const firebaseConfig = {
  apiKey: "AIzaSyAAmKRQGnuXUDZFN3-cwXaez3ooVJev4H4",
  authDomain: "galeryapp-7d096.firebaseapp.com",
  databaseURL: "https://galeryapp-7d096.firebaseio.com",
  projectId: "galeryapp-7d096",
  storageBucket: "galeryapp-7d096.appspot.com",
  messagingSenderId: "617331419255"
};



@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SubirPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    PipesModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SubirPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFireDatabase,
    Camera,
    ImagePicker,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    CargaArchivoProvider
  ]
})
export class AppModule { }
